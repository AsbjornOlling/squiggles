module Squiggles exposing (Model, Msg, init, subscriptions, update, view)

import Browser.Dom
import Browser.Events
import Element
import Element.Background as Background
import Element.Border as Border
import Element.Font as Font
import Element.Input as Input
import Html
import Json.Decode as JD
import List exposing (map)
import Random
import Random.Extra
import Round
import Svg
import Svg.Attributes exposing (height, points, width)
import Task
import Time


type alias Model =
    { size : Size
    , tick : Float
    , groups : List SquiggleGroup
    , hideconf : Bool
    }



{- INIT -}


init : Size -> ( Model, Cmd Msg )
init size =
    ( { size = size
      , tick = 16 -- 1 / 16 ms ~= 60Hz
      , groups = []
      , hideconf = False
      }
        |> addGroup
    , getWindowSize Resize
    )



{- SUBSCRIPTIONS -}


subscriptions : Model -> Sub Msg
subscriptions model =
    -- TODO subscribe to window resize
    Sub.batch
        [ Time.every model.tick Tick
        , Browser.Events.onResize
            (\w h -> Resize { width = toFloat w, height = toFloat h })
        , Browser.Events.onKeyPress
            (JD.field "key" JD.string |> JD.map Key)
        ]



{- UPDATE -}


type Msg
    = Tick Time.Posix
    | WiggledGroups (List SquiggleGroup)
    | Update Model
    | Reset
    | Resize Size
    | Key String


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        Tick _ ->
            ( model
            , Random.generate WiggledGroups <| wiggleGroups model.groups
            )

        WiggledGroups wiggled ->
            ( wiggled
                |> map (growGroup model)
                |> (\gs -> { model | groups = gs })
            , Cmd.none
            )

        Update newmodel ->
            ( newmodel, Cmd.none )

        Reset ->
            ( { model | groups = map (resetGroup model.size) model.groups }
            , Cmd.none
            )

        Resize size ->
            ( { model | size = size }, Cmd.none )

        Key k ->
            if k == " " then
                ( { model | hideconf = not model.hideconf }, Cmd.none )

            else
                ( model, Cmd.none )



{- ANGLE -}


type alias Angle =
    Float


up : Angle
up =
    -pi / 2


right : Angle
right =
    0


down : Angle
down =
    pi / 2


left : Angle
left =
    pi



{- SIZE -}


type alias Size =
    { height : Float
    , width : Float
    }


getWindowSize : (Size -> msg) -> Cmd msg
getWindowSize msg =
    -- get the viewport size sent back as a WindowSizeMsg
    Task.perform
        (\{ viewport } ->
            msg
                { width = viewport.width
                , height = viewport.height
                }
        )
        Browser.Dom.getViewport



{- SQUIGGLEGROUP -}


type alias SquiggleGroup =
    { squiggles : List Squiggle
    , anglemap : List Angle -> List Angle
    , config : GroupConfig
    , num : Int
    }


type alias GroupConfig =
    -- settings used to make a group
    { count : Int
    , speed : Float
    , startangle : Angle
    , angledelta : Angle
    , formation : Formation
    , anglemap : List Angle -> List Angle
    }


defaultGroupConfig : GroupConfig
defaultGroupConfig =
    { count = 256
    , speed = 10
    , formation = Horizontal 0
    , anglemap = List.sort
    , startangle = down
    , angledelta = pi / 256
    }


makeGroup : Size -> GroupConfig -> Int -> SquiggleGroup
makeGroup size conf i =
    { squiggles = makeSquiggles size conf
    , anglemap = conf.anglemap
    , config = conf
    , num = i
    }


resetGroup : Size -> SquiggleGroup -> SquiggleGroup
resetGroup size group =
    { group | squiggles = makeSquiggles size group.config }


addGroup : Model -> Model
addGroup model =
    let
        newgroup =
            makeGroup model.size defaultGroupConfig <|
                (model.groups
                    |> map .num
                    |> List.maximum
                    |> Maybe.withDefault 0
                    |> (+) 1
                )
    in
    { model | groups = newgroup :: model.groups |> List.sortBy .num }


removeGroup : Model -> Int -> Model
removeGroup model num =
    { model | groups = model.groups |> List.filter (\g -> g.num /= num) }


growGroup : Model -> SquiggleGroup -> SquiggleGroup
growGroup model group =
    { group | squiggles = map (growSquiggle model) group.squiggles }



{- FORMATION -}


type
    Formation
    -- TODO Circle
    = Vertical Float
    | Horizontal Float


makeSquiggles : Size -> GroupConfig -> List Squiggle
makeSquiggles size conf =
    let
        is =
            List.range 0 (conf.count - 1)
                |> map toFloat

        origins =
            case conf.formation of
                Vertical x ->
                    is
                        |> map (\i -> i * (size.height / toFloat conf.count))
                        |> map (\y -> Point x y)

                Horizontal y ->
                    is
                        |> map (\i -> i * (size.width / toFloat conf.count))
                        |> map (\x -> Point x y)
    in
    origins
        |> map
            (\p ->
                { origin = p
                , history = []
                , speed = conf.speed
                , angle = conf.startangle
                }
            )


replaceConfig : List SquiggleGroup -> GroupConfig -> GroupConfig -> List SquiggleGroup
replaceConfig groups oldconf newconf =
    case groups of
        g :: gs ->
            if g.config == oldconf then
                { g | config = newconf } :: gs

            else
                g :: replaceConfig gs oldconf newconf

        [] ->
            []


updateGroupConf : Model -> GroupConfig -> GroupConfig -> Model
updateGroupConf model oldconf newconf =
    { model | groups = replaceConfig model.groups oldconf newconf }



{- SQUIGGLE -}


type alias Point =
    { x : Float, y : Float }


type alias Squiggle =
    { origin : Point
    , history : List Point
    , speed : Float
    , angle : Angle
    }


lastPoint : Squiggle -> Point
lastPoint sq =
    List.head sq.history
        |> Maybe.withDefault sq.origin


nextPoint : Squiggle -> Point
nextPoint sq =
    let
        last =
            lastPoint sq
    in
    { last
        | x = last.x + sq.speed * cos sq.angle
        , y = last.y + sq.speed * sin sq.angle
    }


shouldStop : Model -> Squiggle -> Bool
shouldStop model sq =
    let
        last =
            lastPoint sq
    in
    -- primitive: stop as soon as out of bounds
    (last.x < 0)
        || (last.x > model.size.width)
        || (last.y < 0)
        || (model.size.height < last.y)


adjustAngle : Squiggle -> Angle -> Squiggle
adjustAngle sq angledelta =
    { sq | angle = sq.angle + angledelta }


growSquiggle : Model -> Squiggle -> Squiggle
growSquiggle model sq =
    if shouldStop model sq then
        sq

    else
        { sq | history = nextPoint sq :: sq.history }



{- WIGGLE -}


wiggleSquiggle : Float -> Squiggle -> Random.Generator Squiggle
wiggleSquiggle angledelta sq =
    Random.float -1 1
        |> Random.map (\x -> x * angledelta)
        |> Random.map (adjustAngle sq)


wiggleGroup : SquiggleGroup -> Random.Generator SquiggleGroup
wiggleGroup group =
    group.squiggles
        |> map (wiggleSquiggle group.config.angledelta)
        |> Random.Extra.sequence
        |> Random.map (\sqs -> { group | squiggles = sqs })


wiggleGroups : List SquiggleGroup -> Random.Generator (List SquiggleGroup)
wiggleGroups groups =
    groups
        |> map wiggleGroup
        |> Random.Extra.sequence



{- VIEW -}


view : Model -> Html.Html Msg
view model =
    Element.layout
        [ Element.height Element.shrink
        , Element.width Element.shrink
        , Font.family [ Font.monospace ]
        , Font.size 15
        , Element.inFront <|
            if model.hideconf then
                Element.none

            else
                configForm model
        ]
        (Element.column [] [ render model ])



{- SVG -}


render : Model -> Element.Element Msg
render model =
    (model.groups
        |> map .squiggles
        |> List.concat
        |> map (viewSquiggle model)
    )
        |> Svg.svg
            [ width <| String.fromFloat model.size.width
            , height <| String.fromFloat model.size.height
            ]
        |> Element.html


viewSquiggle : Model -> Squiggle -> Svg.Svg Msg
viewSquiggle model sq =
    Svg.polyline
        [ sq.history
            -- TODO: this append is expensive
            ++ [ sq.origin ]
            |> toPointString
            |> points
        , Svg.Attributes.stroke "black"
        , Svg.Attributes.fill "none"
        ]
        []


toPointString : List Point -> String
toPointString ps =
    ps
        |> map (\{ x, y } -> String.fromFloat x ++ ", " ++ String.fromFloat y)
        |> String.join " "



{- UI COMPONENTS -}


button : String -> Bool -> Maybe Msg -> Element.Element Msg
button buttonText active onPress =
    Input.button
        [ Element.padding 5
        , Element.centerX
        , Border.color <| Element.rgb 0 0 0
        , Border.width
            (if active then
                2

             else
                0
            )
        ]
        { onPress = onPress
        , label = Element.text buttonText
        }


slider : { text : String, onChange : Float -> Model, value : Float, min : Float, max : Float, step : Float } -> Element.Element Msg
slider { text, onChange, value, min, max, step } =
    Input.slider
        [ -- Background.color <| sliderColor
          Element.behindContent
            (Element.el
                [ Element.width Element.fill
                , Element.height (Element.px 2)
                , Element.centerY
                , Background.color (Element.rgb 0 0 0)
                ]
                Element.none
            )
        ]
        { label = Input.labelAbove [] (Element.text text)
        , onChange = Update << onChange
        , min = min
        , max = max
        , value = value
        , step = Just step
        , thumb = Input.defaultThumb
        }



{- ALL CONFIG -}


sliderColor : Element.Color
sliderColor =
    Element.rgb 0.1 0.1 0.1


configForm : Model -> Element.Element Msg
configForm model =
    Element.wrappedRow
        [ Element.spacing 20
        , Element.padding 20
        , Element.centerX
        , Element.centerY
        , Border.width 4
        , Border.color <| Element.rgb 0 0 0
        , Background.color <| Element.rgb 1 1 1
        ]
    <|
        List.concat
            [ [ modelConfigForm model ]
            , map (groupConfigForm model) model.groups
            , [ addGroupButton model ]
            ]


modelConfigForm : Model -> Element.Element Msg
modelConfigForm model =
    Element.column
        [ Element.spacing 20, Element.height Element.fill ]
        [ Element.el [ Element.centerY, Element.centerX ] <| resetButton
        , Element.el [ Element.centerY, Element.centerX ] <| tickSlider model
        , Element.el [ Element.alignBottom ] <| Element.text "[SPACE] to hide"
        ]


tickSlider : Model -> Element.Element Msg
tickSlider model =
    slider
        { text = "Tick (ms): " ++ Round.round 0 model.tick
        , onChange = \x -> { model | tick = x }
        , min = 0
        , max = 500
        , value = model.tick
        , step = 5
        }


resetButton : Element.Element Msg
resetButton =
    button "Draw again" True <| Just Reset


addGroupButton : Model -> Element.Element Msg
addGroupButton model =
    button "+" True <| Just (Update <| addGroup model)



{- GROUP CONFIG -}


groupConfigForm : Model -> SquiggleGroup -> Element.Element Msg
groupConfigForm model group =
    Element.column
        [ Element.spacing 20
        , Element.padding 10
        , Border.color <| Element.rgb 0 0 0
        , Border.width 2
        ]
        [ Element.row [ Element.width Element.fill ]
            [ Element.text <| "Group " ++ String.fromInt group.num
            , Input.button
                [ Element.alignRight ]
                { onPress = Just (Update <| removeGroup model group.num)
                , label = Element.text "X"
                }
            ]
        , angleDeltaSlider model group.config
        , speedSlider model group.config
        , countSlider model group.config
        , startAngleSlider model group.config
        , formationSelector model group.config
        ]


formationSelector : Model -> GroupConfig -> Element.Element Msg
formationSelector model conf =
    let
        options =
            [ ( "↤", Vertical model.size.width, pi )
            , ( "↧", Horizontal 0, pi / 2 )
            , ( "↥", Horizontal model.size.height, -pi / 2 )
            , ( "↦", Vertical 0, 0 )
            ]
    in
    Element.row
        [ Element.width Element.fill
        , Element.spacing 20
        ]
        (options
            |> map
                (\( symbol, form, angle ) ->
                    button
                        symbol
                        (conf.formation /= form)
                        (Just <|
                            Update <|
                                updateGroupConf
                                    model
                                    conf
                                    { conf | formation = form, startangle = angle }
                        )
                )
        )


angleDeltaSlider : Model -> GroupConfig -> Element.Element Msg
angleDeltaSlider model conf =
    slider
        { text = "Wiggle amount: " ++ Round.round 2 conf.angledelta
        , onChange = \x -> { conf | angledelta = x } |> updateGroupConf model conf
        , min = 0
        , max = pi / 4
        , step = 0.01
        , value = conf.angledelta
        }


countSlider : Model -> GroupConfig -> Element.Element Msg
countSlider model conf =
    slider
        { text = "Number of lines: " ++ String.fromInt conf.count
        , onChange = \x -> { conf | count = round x } |> updateGroupConf model conf
        , min = 0
        , max = 1000
        , step = 1
        , value = toFloat conf.count
        }


speedSlider : Model -> GroupConfig -> Element.Element Msg
speedSlider model conf =
    slider
        { text = "Line speed: " ++ String.fromFloat conf.speed
        , onChange = \x -> { conf | speed = x } |> updateGroupConf model conf
        , min = 0
        , max = 100
        , step = 1
        , value = conf.speed
        }


startAngleSlider : Model -> GroupConfig -> Element.Element Msg
startAngleSlider model conf =
    slider
        { text = "Angle (radians): " ++ Round.round 2 conf.startangle
        , onChange = \x -> { conf | startangle = x } |> updateGroupConf model conf
        , min = -pi
        , max = pi
        , step = 0.01
        , value = conf.startangle
        }
