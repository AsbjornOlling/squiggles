module Main exposing (..)

import Browser exposing (application)
import Browser.Navigation
import Html
import Squiggles
import Url



{- there really isn't any good reason this file exists.
   tbh it's a totally pointless wrapper around the Squiggles module
   I figured I would make more modules, but I totally haven't (yet, anyway..).
   If you're here to read code, just head straight to Squiggles..
-}


main : Program () Model Msg
main =
    Browser.application
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        , onUrlChange = UrlChanged
        , onUrlRequest = LinkClicked
        }



{- MODEL -}


type Model
    = Squiggles Squiggles.Model
    | Blank


init : () -> Url.Url -> Browser.Navigation.Key -> ( Model, Cmd Msg )
init _ _ _ =
    let
        ( model, cmd ) =
            Squiggles.init { width = 1600, height = 900 }
    in
    ( Squiggles <| model
    , Cmd.map SquiggleMsg cmd
    )



{- SUBSCRIPTIONS -}


subscriptions : Model -> Sub Msg
subscriptions model =
    case model of
        Blank ->
            Sub.none

        Squiggles sqm ->
            Sub.map SquiggleMsg <|
                Squiggles.subscriptions sqm



{- UPDATE -}


type Msg
    = NoOp
    | UrlChanged Url.Url
    | LinkClicked Browser.UrlRequest
    | SquiggleMsg Squiggles.Msg


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case ( msg, model ) of
        ( SquiggleMsg sqmsg, Squiggles sqmodel ) ->
            let
                ( newmodel, newcmd ) =
                    Squiggles.update sqmsg sqmodel
            in
            ( Squiggles newmodel, Cmd.map SquiggleMsg newcmd )

        ( _, _ ) ->
            ( model, Cmd.none )



{- VIEW -}


view : Model -> Browser.Document Msg
view model =
    { title = "YEAH"
    , body =
        [ case model of
            Blank ->
                Html.text ""

            Squiggles sqm ->
                Html.map SquiggleMsg <|
                    Squiggles.view sqm
        ]
    }
